# Polarscope

a [polar](https://en.wikipedia.org/wiki/Polar_coordinate_system) alternative to old-fashioned [cartesian](https://en.wikipedia.org/wiki/Cartesian_coordinate_system) oscilloscopes

The Device is operating fully analog and consists of a quadrature oscillator and two analog multipliers:

![](./diagram.svg)

It can be used to visualize music and to generate polar patterns such as [polar Roses](https://en.wikipedia.org/wiki/Rose_(mathematics))
The resulting shapes can be viewed with an Oscilloscope in X/Y-mode

With no input signal applied and radius offset enabled a circle is displayed
The frequency at which the circle is drawn can be set by a input voltage
The rotation is the time base of the oscilloscope analogous to the left-right sweep of a cartesian oscilloscope

The input signal changes the circle's radius while it is being drawn.
Very slow signals result in growing/shrinking circles
As the input signal approaches the frequency of rotation shapes are drawn
